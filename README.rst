xlogger
-------

This package is used for creating a structured log for your applications.

It is most useful for tracking nested function calls.

It also can be used to track a huge number of class/function parameters and organize them in a better way than a simple text file.

The output will always be an xml file that is intended for use with an XML visualizer. Below is an example where function1 calls function2.


PYTHON

@xlog()
def function1(param):
    print(param, tag="notmsg"
    function2()

@log()
def function2(param):
    print(0)
    print(param)

function1()


XML

< function1 >
    <notmsg "abc" />
    <function2>
        <msg 0 />
        <msg "abc" />
    </function2>
</ function1 >


