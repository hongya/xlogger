from __future__ import print_function

import os, sys, datetime, time, random, multiprocessing, multiprocessing.pool

try:
    import __builtin__
except ImportError:
    # Python 3
    import builtins as __builtin__


# to use Xlogger, you must create an instance of Xlogger and import the xlog and print functions

class xlogger():
    # primary class. it overrides std.out
    # it also saves a log as text in the current directory of the python file that runs it

    def __init__(self, filename, filetype='xml', directory=os.getcwd(), defaultrecipients=[], showconfig=False, replacelog = False):
        basefilename = str(filename)
        newfilename = str(filename)
        self.filetype = filetype


        if replacelog:
            try:
                os.remove((directory + '\%s.%s' % (newfilename, self.filetype)))
            except:
                pass



        i = 0
        while os.path.isfile(directory + '\%s.%s' % (newfilename, self.filetype)):
            i += 1
            newfilename = basefilename + str(i)

        self.terminal = sys.stdout
        self.log = open(directory + '\%s.%s' % (newfilename, self.filetype), "a")
        filepath = directory + '\%s.%s' % (newfilename, self.filetype)
        self.filepath = filepath
        self.filename = basefilename
        self.directory = directory
        self.defaultrecipients = defaultrecipients
        sys.stdout = self

        if showconfig:
            self.terminal.write('Initialized Logger - Saving log to %s \n' % (filepath))

        self.xmlfordev = ''
        self._writexml('<?xml version="1.0" encoding="utf-8"?>')
        self._writexml('''<log>''')



    def write(self, message, **kwargs):

        if not kwargs.get('xmlonly'):
            self.terminal.write(message)

        if not kwargs.get('notline') and message != '\n':
            message = xmlmanager().linetag(message, **kwargs)

        self._writexml(message)

    def _writexml(self, xmessage):

        self.log.write(xmessage)
        self.log.write('\n')

        self.xmlfordev += xmessage

    def close(self):
        self.log.close()

    def flush(self):
        pass

    def __del__(self):
        self._writexml('</log>')


class xmlmanager():
    # xml formatting class. helps prevent formatting errors

    def __init__(self, tag = 'msg', valuesonly=False, nocheckclose=False, **kwargs):
        self._closed = True
        self.nocheckclose = nocheckclose
        self.valuesonly = valuesonly
        self.tag = tag
        return super().__init__(**kwargs)

    def linetag(self, message, tag='msg', **kwargs):
        att = kwargs.get('attributes')
        attstring = self._convertattributes(att)
        return '<%s %s> %s </%s>' % (tag, attstring, message, tag)

    def starttag(self, **kwargs):
        att = kwargs.get('attributes')
        attstring = self._convertattributes(att)
        starttag = '<%s %s>' % (self.tag, attstring)

        if self.valuesonly:
            return starttag
        else:
            print(starttag, xmlonly=True, notline=True)
            self._closed = False
            return starttag

    def endtag(self, **kwargs):

        endtag = '</%s>' % self.tag

        if self.valuesonly:
            return endtag
        else:
            print(endtag, xmlonly=True, notline=True)
            self._closed = True
            return endtag

    def _convertattributes(self, attributes=dict()):
        attstring = ''
        if attributes:
            for key, value in attributes.items():
                attstring = attstring + (' %s = "%s" ' % (key, value))
        return attstring

    def __del__(self):

        if not self._closed:
            self.endtag()

        if not self.nocheckclose:
            assert self._closed, 'An error occurred in the decorated function or xmlmanager class'

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass


def print(message, **kwargs):
    # overloaded print
    sys.stdout.write(str(message), **kwargs)
    sys.stdout.write('\n', **kwargs)


def xlog(hasclass=False, attributes={}, classattributes=[], **deckwargs):
    # decorator for specifying logging behaviour of a function
    # pass attributes to the decorator via a dict and they will become xml tag properites
    # use hasclass = True if you want to include the class name for class methods
    # type in class properties as a list of strings to extract the class property from self
    # the classattributes sends a getattr call

    def superwrapper(func):
        def wrapper(*args, **kwargs):
            prepend = 'F.'

            if hasclass:
                classinstance = args[0]
                prepend = 'C.%s.F.' % (classinstance.__class__.__name__)

                for classattribute in classattributes:
                    attributes[classattribute] = getattr(classinstance, classattribute)

            deckwargs['attributes'] = attributes
            with xmlmanager(prepend + func.__name__) as x:
                x.starttag(**deckwargs)
                func(*args, **kwargs)
                x.endtag()

        return wrapper

    return superwrapper


class testclass():

    # specify that this function create a nested log
    @xlog(hasclass=True)
    def __init__(self, **kwargs):
        self.classprop = 'class prop'
        self.randomprop = random.uniform(0, 1)

        # add output to xml, but don't show on console
        print(self.randomprop, tag='randomprop', xmlonly=True)

        return super().__init__(**kwargs)

    @xlog(hasclass=True)
    def runtests(self, **kwargs):
        # these prints will be nested
        print('beginning tests')
        self.classfun(**kwargs)
        self.multiprocessingtest()
        self.classfun(**kwargs)
        testfun()
        print('ending tests', tag="notmsg")

    # specify nesting and also request a class property and assign an attribute
    @xlog(hasclass=True, classattributes=['classprop'], attributes={'run_time': datetime.datetime.now()})
    def classfun(self, testarg, **kwargs):
        print('before running the nested class function')
        print('check testarg', attributes={'testarg': testarg})

        self.classfunrand()

    @xlog(hasclass=True, classattributes=['randomprop'])
    def classfunrand(self, **kwargs):
        # verifies printing occurred after waiting
        print('first print', attributes={'time': datetime.datetime.now()})
        time.sleep(self.randomprop)
        print('second print after waiting', attributes={'time': datetime.datetime.now()})




    @xlog(hasclass=True)
    def multiprocessingtest(self):
        # mp results print to console, but xlog can't record output from separate processes

        poolsize = 1
        p = multiprocessing.pool.Pool(poolsize)

        jobparams = []
        for i in range(5):
            jobparams.append((random.uniform(0, 1),))

        poolresults = p.starmap(mptest, jobparams)
        p.close()
        p.join()

        print('results must be merged back to the primary process first')


    @xlog(hasclass=True)
    def testnestederror(self):
        self.classfunrand()
        self.testerror()


    @xlog(hasclass=True)
    def testerror(self):
        assert False



# must always type @xlog() brackets are necessary
@xlog()
def testfun():
    print('not a class function')


def mptest(value):
    time.sleep(value)
    print(value)


if __name__ == '__main__':
    logger = xlogger('test', replacelog = True)
    a = testclass()
    a.runtests(testarg='this is the test argument')

    try:
        a.testerror()
        a.testnestederrort
    except:
        print('error')




