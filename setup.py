from setuptools import setup

def readme():
    with open('README.rst') as f:
        return f.read()


setup(name='xlogger'
      ,version='0.1'
      ,description='nested logging library. useful for logging large parameters for your applications, such as sql queries.'
      ,long_description=readme()
      ,url= 'none'
      ,author = 'hongy'
      , author_email= 'hongy@gmail.com'
      , license = 'MIT'
      ,packages = 'xlogger'
      ,zip_safe = False
      )